# GRC Contact CRM SDK for PHP (v1)


This repository contains the open source PHP SDK that allows you to access the GRC Contact CRM Platform from your PHP app.

## Installation

The GRC Contact CRM PHP SDK can be installed with [Composer](https://getcomposer.org/). Run this command:

```sh
composer require grccontact/php-sdk
```

## Usage

> **Note:** This version of the GRC Contact CRM SDK for PHP requires PHP 5.4 or greater.

Simple GET example of a user's profile.

```php
require_once __DIR__ . '/vendor/autoload.php'; // change path as needed

$grc = new \Grccontact\Grccontact([
  'personnal-acces-token' => '{token}',
  'api-version' => 'v3'
]);


try {
  $response = $grc->get('{endpoint}');
} catch(\Exceptions $e) {
  echo 'GRC Contact CRM returned an error: ' . $e->getMessage();
  exit;
}

$api = $response->getData();
echo 'Value : ' . $api->getProperty();
```

Complete documentation, installation instructions, and examples are available [here](docs/).

## TODO