<?php
/**
 * Copyright 2020 GRC Contact.
 * 
 * Here the description
 */
namespace Grccontact;

use Carbon\Carbon;
use Guzzle\Client;

use Grccontact\EntityServices\OrganizationService;

/**
 * Class Grccontact
 *
 * @package Grccontact
 */
class Grccontact
{
    /**
     * @const string Version number of the GRC Contact PHP SDK.
     */
    const VERSION = '1';

    /**
     * @const string Default API version for requests.
     */
    const DEFAULT_API_VERSION = 'v3';

    /**
     * @const string The name of the environment variable that contains the access token.
     */
    const APP_ACCES_TOKEN = 'GRCCONTAC_ACCESS_TOKEN';


    /**
     * Dependencies
     */
    use OrganizationService;

    /**
     * Instantiates a new Grccontact super-class object.
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $config = array_merge([
            'access_token' => getenv(static::APP_ACCES_TOKEN)
            'api_version' => static::DEFAULT_API_VERSION
        ], $config);
    }

    
    /**
     * Sends a GET request to API and returns the result.
     *
     * @param string                  $endpoint
     * @param AccessToken|string|null $accessToken
     * @param string|null             $version
     *
     * @return 
     */
    public function get($endpoint, $accessToken = null, $version = null)
    {
        return $this->sendRequest(
            'GET',
            $endpoint,
            $params = [],
            $accessToken,
            $version
        );
    }

    /**
     * Sends a POST request to API and returns the result.
     *
     * @param string                  $endpoint
     * @param array                   $params
     * @param AccessToken|string|null $accessToken
     * @param string|null             $version
     *
     * @return
     */
    public function post($endpoint, array $params = [], $accessToken = null, $version = null)
    {
        return $this->sendRequest(
            'POST',
            $endpoint,
            $params,
            $accessToken,
            $version
        );
    }

    /**
     * Sends a DELETE request to API and returns the result.
     *
     * @param string                  $endpoint
     * @param array                   $params
     * @param AccessToken|string|null $accessToken
     * @param string|null             $version
     *
     * @return
     */
    public function delete($endpoint, array $params = [], $accessToken = null, $version = null)
    {
        return $this->sendRequest(
            'DELETE',
            $endpoint,
            $params,
            $accessToken,
            $version
        );
    }

    /**
     * Sends a request to API and returns the result.
     *
     * @param string                  $method
     * @param string                  $endpoint
     * @param array                   $params
     * @param AccessToken|string|null $accessToken
     * @param string|null             $version
     *
     * @return
     */
    public function sendRequest($method, $endpoint, array $params = [], $accessToken = null, $version = null)
    {
        // TODO
    }
}
